This repository contains information related with the publication entitled: Establishment and assessment of an amplicon sequencing method targeting the 16S-ITS-23S rRNA operon for analysis of the equine gut microbiome, Yuta Kinoshita.

File description:

"ncbi_202006.fasta.gz" contains the sequence database of bactrial and archaeal rRNA genes which were retrieved from NCBI database in June 2020.
